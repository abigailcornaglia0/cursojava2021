package modulo3;

import java.util.Scanner;

public class ejercicio18 {

	public static void main(String[] args) 
	{
		System.out.println("Ingrese un n�mero para ver su tabla.");
		Scanner scan = new Scanner(System.in);
		int numero = scan.nextInt();
		
		for(int i=1; i<numero+1; i++ )
		{
			System.out.println("Tabla del " + i);
			for(int a=1; a<11; a++ )
			{
				int respuesta = i * a;
				System.out.println(i + "x" + a + "=" + respuesta);
			}
			
		}
	}

}