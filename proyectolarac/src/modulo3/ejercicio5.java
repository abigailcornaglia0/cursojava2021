package modulo3;

import java.util.Scanner;

public class ejercicio5 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese el puesto obtenido");
		int puesto =scan.nextInt();
		
		if(puesto == 1)
			System.out.println("Usted gano la medalla de oro.");
		else if(puesto == 2)
			System.out.println("Usted gano la medalla de plata.");
		else if(puesto == 3)
			System.out.println("Usted gano la medalla de bronce.");
		else
			System.out.println("Usted no gano ninguna medalla, siga participando.");
		
		scan=null;

	}

}