package modulo3;

import java.util.Scanner;

public class ejercicio13 {

	public static void main(String[] args) 
		{
			Scanner scan = new Scanner(System.in);
			System.out.println("Ingrese un numero de mes");
			int mes = scan.nextInt();
			
			switch (mes)
			{
			case 1: case 3: case 5: case 7: case 8: case 10: case 12:
				System.out.println("El mes ingresado tiene 31 dias");
				break;
			case 2: 
				System.out.println("El mes ingresado tiene 28 dias");
				break;
			case 4: case 6: case 9: case 11:
				System.out.println("El mes ingresado tiene 30 dias");
				break;
			
			default:
				System.out.println("El n�mero es inv�lido");
				break;
			}
			scan=null;
		}
	}