package modulo3;

import java.util.Scanner;

public class ejercicio12 {

	public static void main(String[] args) {
	
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un n�mero entero");
		int n = scan.nextInt();
		
		if (n >= 1 && n <= 12)
			System.out.println("El n�mero pertenece a la 1er docena.");
		else if (n >= 12 && n <= 24)
			System.out.println("El n�mero pertenece a la 2da docena.");
		else if (n >= 24 && n <= 36)
			System.out.println("El n�mero pertenece a la 3er docena.");
		else if (n < 1 || n > 36 )
			System.out.println("El n�mero ingresado es inv�lido");
		
		scan=null;
	}
}