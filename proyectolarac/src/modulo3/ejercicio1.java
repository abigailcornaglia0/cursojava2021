package modulo3;

import java.util.Scanner;

public class ejercicio1 {

	public static void main(String[] args) {
				
			Scanner scan = new Scanner(System.in);
			System.out.println("Ingrese primer nota:");
			float nota1=scan.nextFloat();
			System.out.println("Ingrese segunda nota:");
			float nota2=scan.nextFloat();
			System.out.println("Ingrese tercer nota:");
			float nota3=scan.nextFloat();
			
			float promedio=(nota1+nota2+nota3)/3;
			
			if (promedio >=7)
			{
				System.out.println("El alumno aprobo con promedio de " + promedio);
			} 
			else
			{
				System.out.println("El alumno desaprobo con promedio de " + promedio);
			}
			scan=null;
	}

}
