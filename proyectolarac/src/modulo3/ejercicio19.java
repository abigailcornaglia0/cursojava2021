package modulo3;

public class ejercicio19
{
	public static void main(String[] args) 
	{
		int numero = 0;
		int suma = 0;
		int promedio = 0;
			
		while (numero<11)
		{
			int numeroaleatorio = (int) Math.floor(Math.random()*1000);
			System.out.println("El numero aleatoriamente generado es " + numeroaleatorio);	
			suma = suma + numeroaleatorio;
			numero = numero + 1;
		}
		System.out.println("La suma de los numeros aleatorios es " + suma);
		
		promedio = suma/10;
		
		System.out.println("El promedio de estas sumas es " + promedio);	
	}
}