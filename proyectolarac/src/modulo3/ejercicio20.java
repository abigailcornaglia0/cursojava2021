package modulo3;

public class ejercicio20
{
	public static void main(String[] args)
	{
		int numero = 0;
		int mayor = 0;
		int menor = 1001;
		
		while (numero<10)
		{
			int numeroaleatorio = (int) Math.floor(Math.random()*1000);
			System.out.println("El numero aleatoriamente generado es " + numeroaleatorio);	
			if (numeroaleatorio < menor)
				menor = numeroaleatorio;
			if (numeroaleatorio > mayor)
				mayor = numeroaleatorio;
			numero = numero + 1;
		}
		System.out.println("El menor numero es " + menor);
		
		System.out.println("El mayor numero es " + mayor);	
	}

}
