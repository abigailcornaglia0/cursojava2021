package modulo3;

import java.util.Scanner;

public class ejercicio14 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Ingrese el numero de puesto");
		int puesto = scan.nextInt();
		
		switch (puesto)
		{
		case 1:
			System.out.println("Usted gan� la medalla de oro.");
			break;
		case 2:
			System.out.println("Usted gan� la medalla de plata.");
			break;
		case 3:
			System.out.println("Usted gan� la medalla de bronce.");
			break;
		default:
			System.out.println("Usted no gan� ninguna medalla, siga participando.");
			break;
		}
		scan=null;
	}
}
